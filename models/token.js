'use strict';
module.exports = function(sequelize, DataTypes) {
  var Token = sequelize.define('Token', {
    access_token: DataTypes.STRING,
    expires_in: DataTypes.INTEGER,
    refresh_token: DataTypes.STRING
  }, {
    tableName: 'tokens',
    classMethods: {
      associate: function(models) {
        Token.belongsTo(models.User, {
          foreignKey: {
            allowNull : false
          }
        });
      }
    }
  });
  return Token;
};