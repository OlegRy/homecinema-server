var models = require('../models');
var uuid = require('uuid-v4');

exports.getAll = function(req, res) {
	var genre = req.query.genre.toUpperCase();
	console.log(genre);
	if (!genre || genre === undefined) {
		res.status(401).send({ message : 'bad request' });
	} else {
		models.Movie.findAll( {
			where: {
				genre : genre
			},
			include: [models.Room]
		}).then(function(movies) {
			res.json(movies);
		});	
	}
}

exports.getGenres = function(req, res) {
	models.Movie.findAndCountAll({
		attributes: ['genre'],
		group: ['genre']
	}).then(function(result) {
		res.status(200).send({ genres : result.rows });
	});
}

exports.getUrl = function(req, res) {
	var id = req.params.movie_id;
	models.Movie.findById(id).then(function(movie) {
		res.send(movie.url)
	});
};

exports.findRoom = function(req, res) {
	//-console.log(req);
	var id = req.query.movie_id;
	var room_name = req.query.room_name;
	models.Room.findOne( {
		where: {
			name : room_name,
			MovieId : id
		}
	}).then(function(room) {
		var answer = room ? true : false;
		res.json({ exists : answer });
		
	});
};

exports.createRoom = function(req, res) {
	var id = uuid();
	var room = req.body;
	var room_name = room.name;
	var movie_id = room.MovieId;
	var created_at = new Date();
	var updated_at = new Date();
	models.Room.create({
		room_id : id,
		name : room_name,
		MovieId : movie_id,
		createdAt : created_at,
		updatedAt : updated_at
	}).then(function(room) {
		res.json(room);
	});
}

exports.createMovie = function(req, res) {
	var movie = req.body;
	var title = movie.title;
	var description = movie.description;
	var duration = movie.duration;
	var genre = movie.genre.toUpperCase();
	var posterUrl = movie.posterUrl;
	var url = movie.url;
	models.Movie.create({
		title : title,
		description : description,
		duration : duration,
		genre : genre,
		posterUrl : posterUrl,
		url : url
	}).then(function(movie) {
		if (movie) res.status(201).json({ message : 'Movie has been added' });
		else res.status(500).json({ message : 'Unable to add movie' });
	});
}