var models = require('../models');
var bcrypt = require('bcrypt');
var crypto = require('crypto');

exports.post = function(req, res) {

	var login = req.query.login;
	if (!login || login === undefined) {
		res.status(400).send({ message : 'login cannot be empty' });
		return;
	}
	var password = req.query.password;
	if (!password || password === undefined) {
		res.status(400).send({ message: 'password cannot be empty' });
		return;
	}
	models.User.findOne({
		where: {
			login : login 
		}
	}).then(function(user) {
		if (!user || user === undefined) {
			res.status(404).send({ message : 'No users found with login ' + login });
		} else if (!user.isModerated) {
			res.status(403).send({ message : 'Account is not moderated yet' });
		} else {
			bcrypt.compare(password, user.password, function(err, result) {
				if (err) throw err;
				if (!result) {
					res.status(403).send({ message : 'password is incorrect' });
				} else {
					models.Token.findOne({
						where: {
							UserId : user.id
						}
					}).then(function(token) {
						if (token && token != undefined) {
							res.status(200).json(token);
						} else {
							crypto.randomBytes(48, function(err, buffer) {
  								var accessToken = buffer.toString('hex');
  								var expiresIn = 24 * 3600;
  								crypto.randomBytes(48, function(err, buffer) {
  									var refreshToken = buffer.toString('hex');
  									models.Token.build({
  										access_token : accessToken,
  										expires_in : expiresIn,
  										refresh_token : refreshToken,
  										UserId : user.id
  									}).save().then(function(token) {
  										res.status(200).json(token);	
  									});
  								});
							});
						}
					});
				}
			});
		}
	})
};