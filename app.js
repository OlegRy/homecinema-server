var express = require("express");
var http = require("http");
var Log = require("log"),
	log = new Log('debug');
var config = require('./config');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var app = new express();

app.set('port', config.get('port'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(__dirname + "/public"));

var routes = require('./routes')(app);

var server = http.createServer(app);

server.listen(app.get('port'), function() {
	log.info("Server started at port %s", config.get('port'));
});

require('./socket')(server);
