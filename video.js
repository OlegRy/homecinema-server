var express = require("express");
var app = new express();
var http = require("http").Server(app);
var io = require("socket.io")(http);
var fs = require("fs");
var Log = require("log"),
	log = new Log('debug');

var port = process.env.PORT || 3000;

app.use(express.static(__dirname + "/public"));

app.get('/', function(request, response) {
	response.redirect('index.html');
});

io.sockets.on('connection', function(socket) {

	var rs = fs.createReadStream("public/files/sample.mp4");
	rs.on('stream', function (data) {
		socket.broadcast.emit.('stream', data);
	});
	/*socket.on('stream', function(fileName) {
		var rs = fs.createReadStream("files/" + fileName);
		//socket.broadcast.emit('stream', image);
	});*/
});

http.listen(port, function() {
	log.info('Server at %s', port);
});