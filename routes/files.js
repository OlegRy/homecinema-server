var multiparty = require('multiparty');
var fs = require('fs');
var models = require('../models');

exports.upload = function(req, res, next) {

	var accessToken = req.get('Authorization');
	if (!accessToken || accessToken === undefined) {
		res.status(401).send({ message : 'Unauthorized' });
	} else {
		models.Token.findOne({
			where : {
				access_token : accessToken
			}
		}).then(function(token) {
			if (!token || token === undefined) {
				res.status(401).send({ message : 'bad token' });
			} else {
				models.User.findOne({
					where: {
						id : token.UserId
					}
				}).then(function(user) {
					if (!user || user === undefined) {
						res.status(404).send({ message : 'user with given token not found '});
					} else if (!user.isDisseminator) {
						res.status(403).send({ message : 'Permission denied' });
					} else {
						startUploading(req, res);		
					}
				})
				
			}
		});
	}
}

function startUploading(req, res) {
	var form = new multiparty.Form();
	var uploadFile = { uploadPath : '', type : '', size : 0};

	var supportMimeTypes = ['video/mp4', 'image/*'];
	var errors = [];

	form.on('error', function(err) {
		if (fs.existsSync(uploadFile.uploadPath)) {
			res.status(409).send({ status : 'file exists' })
		}
	});

	form.on('close', function() {
		if (errors.length === 0) {
			res.status(201).send({ path : uploadFile.uploadFile });
		} else {
			res.status(500).send({ status: 'bad', errors : errors });
		}
	});

	form.on('part', function(part) {
		uploadFile.size = part.byteCount;

		uploadFile.type = part.headers['content-type'];

		if (uploadFile.type === 'video/mp4') uploadFile.uploadPath = './public/files/' + part.filename;
		else uploadFile.uploadPath = './public/files/posters/' + part.filename;

		if (supportMimeTypes.indexOf(uploadFile.type) == -1) {
            errors.push('Unsupported mimetype ' + uploadFile.type);
        }

        if (errors.length == 0) {
            var out = fs.createWriteStream(uploadFile.uploadPath);
            part.pipe(out);
        } else {
            //пропускаем
            //вообще здесь нужно как-то остановить загрузку и перейти к onclose
            part.resume();
        }
	});

	form.parse(req);
}