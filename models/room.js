'use strict';
module.exports = function(sequelize, DataTypes) {
  var Room = sequelize.define('Room', {
    room_id: {
    	primaryKey: true,
    	type: DataTypes.STRING
    },
    name: DataTypes.STRING,
    currentTime: DataTypes.INTEGER
  }, {
  	tableName: 'rooms',
    classMethods: {
      associate: function(models) {
        Room.belongsTo(models.Movie, {
          foreignKey: {
            allowNull: false
          }
        });
      }
    }
  });
  return Room;
};