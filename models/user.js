'use strict'

var bcrypt = require('bcrypt');

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    login: { type : DataTypes.STRING, allowNull : false, unique: true },
    password: { type : DataTypes.STRING, allowNull : false},
    email: { type : DataTypes.STRING, allowNull : false, unique: true },
    avatar: { type : DataTypes.STRING, allowNull : true },
    isDisseminator: { type : DataTypes.BOOLEAN, allowNull : false, defaultValue : false },
    isModerated: { type : DataTypes.BOOLEAN, allowNull : false, defaultValue : false }
  }, {
    tableName : 'users',
    timestamps : false,
    classMethods: {
      associate: function(models) {
        User.hasOne(models.Token, {
          foreignKey: {
            allowNull: false
          }
        })
      }
    },
    hooks : {
      beforeCreate : hashPassword,
      beforeUpdate : hashPassword
    }
  });
  return User;
}

function hashPassword(user, options, cb) {
  console.log('hashPassword');
  return bcrypt.genSalt(10, function(err, salt) {
    return bcrypt.hash(user.password, salt, function(err, encrypted) {
      if (err) return cb(err);
        user.password = encrypted;
          return cb(null, options);
      });
  });
}