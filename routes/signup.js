var models = require('../models');

exports.post = function(req, res) {

	var login = req.query.login;
	if (!login || login === undefined) {
		res.status(400).send({ message : 'login cannot be empty' });
		return;
	}

	var email = req.query.email;
	if (!email || email === undefined) {
		res.status(400).send({ message : 'email cannot be empty' });	
		return;
	}

	var password = req.query.password;
	if (!password || password === undefined) {
		res.status(400).send({ message : 'password cannot be empty' });
		return;
	}

	var avatar = req.query.avatar;

	models.User.findOne({
		where: {
			login : login
		}
	}).then(function(user) {
		if (user) res.status(409).send({ message : 'User already exists' });
		else {
			var email = req.query.email;
			models.User.findOne({
				where: {
					email: email
				}
			}).then(function(user1) {
				if (user1) res.status(409).send({ message : 'User with given email already exists' });
				else {
					var password = req.query.password;
					var disseminator = req.query.isDisseminator;
					var moderated = disseminator ? false : true;
					var newUser = models.User.build({
						login: login,
						password: password,
						email: email,
						avatar: avatar,
						isDisseminator: disseminator,
						isModerated: moderated
					}).save().then(function(anotherTask) {
						res.status(201).send({ message : 'Signing up is successful!' });
					});
				}
			})
		}
	})
};