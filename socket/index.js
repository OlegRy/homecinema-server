module.exports = function(server) {
	var io = require("socket.io").listen(server);
	var models = require('../models');
	var connected = 0;
	var currentId;
	var times;

	io.sockets.on('connection', function(socket) {
		connected++;
		currentId = socket.id;

		socket.emit('room', {});
		
		socket.on('message', function(msg) {
			
			sendTime(socket, msg);
			
		});
		socket.on('disconnect', function() {
			connected--;
			socket.leave(socket.room);
			socket.room = null;
		});
		socket.on('setTime', function(currentTime) {
			sendTime(socket, currentTime);
		});
		socket.on('getTime', function(roomId) {
			var clientInRoom;
			io.sockets.in(roomId).clients(function(error, clients) {
				if (error) throw error;
				console.log(clients);
				clientInRoom = clients[0];
				if (clientInRoom && typeof(clientInRoom) != undefined && clientInRoom != socket.id) {
					console.log("i'm in if block!!!");
					io.sockets.connected[clientInRoom].emit('getTime', {});
				} else {
					console.log("i'm in else block!!!");
					sendTime(socket, 0);
				}
			});
			
		});
		socket.on('join', function(roomId) {
			var clientInRoom;
			io.sockets.in(roomId).clients(function(error, clients) {
				if (error) throw error;
				console.log(clients);
				clientInRoom = clients[0];
				if (clientInRoom && typeof(clientInRoom) != undefined) {
					io.sockets.connected[clientInRoom].emit('getTime', {});
				} else {
					sendTime(socket, 0);
				}
			});
			socket.join(roomId);
			socket.room = roomId;
		});
		socket.on('play_pause', function(playing) {
			socket.broadcast.to(socket.room).emit('play_pause', playing);
		});
		socket.on('seek_to', function(position) {
			socket.broadcast.to(socket.room).emit('seek_to', position);
		});
		socket.on('sync_request', function(roomId) {
			io.sockets.in(roomId).clients(function(error, clients) {
				if (error) throw error;
				console.log(clients);
				if (clients != undefined) {
					var firstClient = clients[0];
					if (firstClient != undefined && firstClient != socket.id) {
						console.log("sending from " + socket.id + " to " + firstClient);
						io.sockets.connected[firstClient].emit('sync_request', socket.id);
					}
				}
			});
		});
		socket.on('sync', function(currentTime, socketId) {
			console.log("sending to " + socketId);
			io.sockets.connected[socketId].emit('sync_response', currentTime);
		});
		socket.on('chat_message', function(message) {
			socket.broadcast.to(socket.room).emit('chat_message', message);
		});
	});

	function sendTime(socket, time) {
		io.sockets.connected[currentId].json.send({'url' : '/files/sample2.mp4', 'time' : time });
	}	
};
