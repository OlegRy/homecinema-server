module.exports = function(app) {

	app.get('/', function(req, res) {
		res.redirect('socket.html');
	});

	app.get('/movies', require('./movies').getAll);

	app.post('/signIn', require('./signin').post);

	app.post('/signUp', require('./signup').post);

	app.get('/roomExists', require('./movies').findRoom);

	app.post('/createRoom', require('./movies').createRoom);

	app.post('/upload', require('./files').upload);

	app.get('/getUser', require('./users').get);

	app.post('/createMovie', require('./movies').createMovie);

	app.put('/uploadAvatar', require('./users').uploadAvatar);

	app.put('/changePassword', require('./users').changePassword);

	app.get('/genres', require('./movies').getGenres);

	app.get('/canControl', require('./users').canControl);
};