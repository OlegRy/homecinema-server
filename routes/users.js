var models = require('../models');
var fs = require('fs');
var multiparty = require('multiparty');
var bcrypt = require('bcrypt');
var uuid = require('uuid-v4');

exports.get = function(req, res) {
	var accessToken = req.get('Authorization');
	models.Token.findOne({
		where: {
			access_token : accessToken
		}
	}).then(function(token) {
		if (!token || token === undefined) res.status(403).send({ message : 'no token' });
		else {
			models.User.findOne({
				where: {
					id : token.UserId
				}
			}).then(function(user) {
				if (!user || user === undefined) res.status(404).send({ message : 'no user with given token' });
				else res.status(200).json({ login : user.login, email : user.email, avatar : user.avatar, isDisseminator : user.isDisseminator });
			});
		}
	});
}

exports.uploadAvatar = function(req, res) {
	uploadImage(req, res);
}

exports.updateAvatar = function(req, res) {
	var accessToken = req.get('Authorization');
	if (!accessToken || accessToken === undefined) {
		res.status(403).send({ message : 'Forbidden' });
		return;
	}
	models.Token.findOne({
		where: {
			access_token : accessToken
		}
	}).then(function(token) {
		if (!token || token === undefined) res.status(403).send({ message : 'no token' });
		else {
			models.User.findOne({
				where: {
					id : token.UserId
				}
			}).then(function(user) {
				if (!user || user === undefined) res.status(404).send({ message : 'no user with given token' });
				else {
					var avatar = user.avatar;
					fs.unlink('./public/' + avatar);
					var newAvatar = req.query.avatar;
					models.User.update({ avatar : newAvatar }, { where : { id : user.id } }).then(function(result) {
						res.status(200).send({ message : 'avatar updated' });
					}, function(reject) {
						res.status(500).send({ message : 'Something went wrong' });
					});
				}
			});
		}
	});
}

exports.changePassword = function(req, res) {
	var login = req.query.login;
	var currentPassword = req.query.currentPassword;
	models.User.findOne({
		where : {
			login : login
		}
	}).then(function(user) {
		if (!user || user === undefined) {
			res.status(404).send({ message : 'User not found' });
		} else {
			bcrypt.compare(currentPassword, user.password, function(err, result) {
				if (err) throw err;
				if (!result) res.status(403).send({ message : 'Current password is incorrect' });
				else {
					models.User.update({ password : req.query.newPassword }, { where: { login : login }, individualHooks : true }).then(function(result) {
						res.status(200).send({ message : 'Password has been changed' });
					}, function(reject) {
						res.status(500).send({ message : 'Something went wrong' });
					});
				}
			});
		}
	});
}

exports.canControl = function(req, res) {
	var accessToken = req.get('Authorization');
	models.Token.findOne({
		where: {
			access_token : accessToken
		}
	}).then(function(token) {
		if (!token || token === undefined) {
			var randomNickId = uuid();
			var nickname = 'Anonymous_' + randomNickId;
			res.status(200).send({ message : 'token not found', canControl : false, nickname : nickname, avatar : null });
		}
		else {
			models.User.findOne({
				where: {
					id : token.UserId
				}
			}).then(function(user) {
				if (!user || user === undefined) {
					var randomNickId = uuid();
					var nickname = 'Anonymous_' + randomNickId;
					res.status(200).send({ message : 'token not found', canControl : false, nickname : nickname, avatar : null });			
				} else {
					var nickname = user.login;
					var avatar = user.avatar;
					res.status(200).send({ message : 'success', canControl : true, nickname : nickname, avatar : avatar });
				}
			});
		}
	});
}

function uploadImage(req, res) {
	var form = new multiparty.Form();
	var uploadFile = { uploadPath : '', type : '', size : 0};

	var supportMimeTypes = ['image/*'];
	var errors = [];

	form.on('error', function(err) {
		if (fs.existsSync(uploadFile.uploadPath)) {
			res.status(409).send({ status : 'file exists' })
		}
	});

	form.on('close', function() {
		if (errors.length === 0) {
			res.status(201).send({ path : uploadFile.uploadPath });
		} else {
			res.status(500).send({ status: 'bad', errors : errors });
		}
	});

	form.on('part', function(part) {
		uploadFile.size = part.byteCount;

		uploadFile.type = part.headers['content-type'];

		uploadFile.uploadPath = './public/files/avatars/' + part.filename;

		if (supportMimeTypes.indexOf(uploadFile.type) == -1) {
            errors.push('Unsupported mimetype ' + uploadFile.type);
        }

        if (errors.length == 0) {
            var out = fs.createWriteStream(uploadFile.uploadPath);
            part.pipe(out);
        } else {
            //пропускаем
            //вообще здесь нужно как-то остановить загрузку и перейти к onclose
            part.resume();
        }
	});

	form.parse(req);
}