'use strict';
module.exports = function(sequelize, DataTypes) {
  var Movie = sequelize.define('Movie', {
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    duration: DataTypes.INTEGER,
    genre: DataTypes.STRING,
    posterUrl: DataTypes.STRING,
    url: DataTypes.STRING
  }, {
  	tableName: 'movies',
  	timestamps: false,
    classMethods: {
      associate: function(models) {
        Movie.hasMany(models.Room)
      }
    }
  });
  return Movie;
};